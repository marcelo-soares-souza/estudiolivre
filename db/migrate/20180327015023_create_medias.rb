class CreateMedias < ActiveRecord::Migration[5.1]
  def change
    create_table :medias do |t|
      t.string :media_type
      t.string :title
      t.string :author
      t.string :description
      t.string :slug
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
