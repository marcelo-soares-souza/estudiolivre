class AddAttachmentFileToMedias < ActiveRecord::Migration[5.1]
  def self.up
    change_table :medias do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :medias, :file
  end
end
