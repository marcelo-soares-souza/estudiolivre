# Estúdio Livre

1) Install Bundler

- gem install bundler

2) Set environment variables with database connection information (see the example below)

- export RAILS_ENV='development'
- export EL_USERNAME_DB='devel'
- export EL_PASSWORD_DB='devel'
- export EL_HOST_DB='localhost'
- export EL_PORT_DB='5432'

2.1) E-Mail Server (Optional)

- export EL_SMTP_SERVER='smtp.example.org'
- export EL_SMTP_PORT='587'
- export EL_SMTP_DOMAIN='example.org'
- export EL_SMTP_USERNAME='user'
- export EL_SMTP_PASSWORD='password'

Rename config/database.yml.example to config/database.yml

3) Init environment

- bundle install
- bundle exec rails db:create
- bundle exec rails db:migrate
- bundle exec rails db:seed

4) Start Rails Server

- bundle exec rails server

--

More info: https://estudiolivre.org

This project is licensed under the terms of the GPLv3
