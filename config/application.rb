require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module Estudiolivre
  class Application < Rails::Application
    config.load_defaults 5.1
    config.generators.test_framework false
  end
end
