Rails.application.routes.draw do
  scope "(:locale)", :locale => /pt-BR|es|en/ do
    root :to => 'home#index'
    get "home/index"
  end

  resources :medias

  devise_for :users
  resources :users

  get 'home/index'
  root to: "home#index"
end
