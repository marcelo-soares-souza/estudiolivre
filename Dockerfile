FROM php:5.6-apache

RUN apt update && apt install tzdata -y
ENV TZ="America/Sao_Paulo"

RUN docker-php-ext-install mysql
RUN a2enmod rewrite
