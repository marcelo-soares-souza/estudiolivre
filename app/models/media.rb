class Media < ApplicationRecord
  belongs_to :user

  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title, presence: true
  validates :media_type, presence: true

  has_attached_file :file
  do_not_validate_attachment_file_type  :file
end
